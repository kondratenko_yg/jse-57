package ru.kondratenko.tm.repository;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
@Repository
public class TaskRepository extends CommonRepository<Task>  implements ITaskRepository  {

    public TaskRepository() {
        super(Task.class);
    }

    @Override
    public Optional<Task> update(Task input) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<Task> entity = Optional.empty();

        try {
            entity = Optional.ofNullable(session.createQuery("UPDATE "
                    + Task.class.getName() + " u set name = ?, description = ?,project = ?, user = ?,deadline = ? where id = ?", Task.class)
                    .setParameter("name", input.getName())
                    .setParameter("description", input.getDescription())
                    .setParameter("project", input.getProject())
                    .setParameter("user", input.getUser())
                    .setParameter("deadline",input.getDeadline())
                    .setParameter("id",input.getId())
                    .getSingleResult());
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();

        return entity;
    }


    @Override
    public List<Task> findByName(String name){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<Task> entity = session.createQuery("SELECT u FROM " + Task.class.getName() + " u where u.name = :name", Task.class)
                    .setParameter("name", name).getResultList();

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public void removeByName(String name)  {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.createQuery("DELETE u FROM " + Task.class.getName() + " u where u.name = :name", Task.class)
                .setParameter("name", name);

        tx.commit();
        session.close();
    }


    @Override
    public List<Task> findAllByUserId(final Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<Task> entity = session.createQuery("SELECT u FROM " + Task.class.getName() + " u inner join u.user as c_user where c_user.id = :id", Task.class)
                .setParameter("id", id).getResultList();

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public Optional<Task> findByProjectIdAndId(Long projectId, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<Task> entity = Optional.empty();

        try {
            entity = Optional.ofNullable(session.createQuery("SELECT u FROM " + Task.class.getName() + " u inner join u.project as c_project where u.id = :id and c_project.id=:project_id", Task.class)
                    .setParameter("id", id)
                    .setParameter("project_id",projectId).getSingleResult());
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public List<Task> findAllByProjectId(Long projectId) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Task> entity= session.createQuery("SELECT u FROM " + Task.class.getName() + " u inner join u.project as c_project where c_project.id=:project_id", Task.class)
                    .setParameter("project_id",projectId).getResultList();

        tx.commit();
        session.close();

        return entity;
    }
}
