package ru.kondratenko.tm.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import ru.kondratenko.tm.entity.AbstractEntity;
import ru.kondratenko.tm.repository.config.HibernateConfig;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

@Repository
public class CommonRepository<T extends AbstractEntity> implements IRepository<T> {

    public Class<T> clazz;

    public CommonRepository(Class<T> clazz) {
        this.clazz = clazz;
        this.sessionFactory = HibernateConfig.getSessionFactory();
    }

    public SessionFactory sessionFactory;

    @Override
    public  Optional<T> create(final T entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
        return Optional.of(entity);
    }

    @Override
    public Optional<T> findById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        T entity = session.get(clazz, id);

        tx.commit();
        session.close();

        return Optional.of(entity);
    }

    @Override
    public List<T> findAll() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<T> items = session.createQuery("SELECT c from "+clazz.getName()+" c", clazz).getResultList();

        tx.commit();
        session.close();

        return items;
    }

    @Override
    public void clear() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.createQuery("SELECT c from "+clazz.getName()+" c", clazz);

        tx.commit();
        session.close();
    }

    @Override
    public Optional<T> findByIndex(int index) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<T> entity = Optional.empty();

        try {
            entity = Optional.ofNullable(session.createQuery("SELECT u FROM "+
            "(SELECT t, row_number()  OVER () as rnum from "+ clazz.getName() + ") u where rnum = :index", clazz)
                    .setParameter("index", index).getSingleResult());
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public void removeByIndex(int index) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {
            session.createQuery("DELETE u FROM "+
                    "(SELECT t, row_number()  OVER () as rnum from "+ clazz.getName() + ") u where rnum = :index", clazz)
                    .setParameter("index", index);
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();
    }

    @Override
    public void removeById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.createQuery("DELETE c from "+clazz.getName()+" c WHERE id=:id", clazz)
                .setParameter("id",id);

        tx.commit();
        session.close();
    }
}
