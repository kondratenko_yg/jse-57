package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    Optional<E> create(final E item);

    Optional<E> findById(final Long id);

    List<E> findAll();

    void clear();

    Optional<E> findByIndex(final int index);

    void removeByIndex(final int index);

    void removeById(final Long id);
}
