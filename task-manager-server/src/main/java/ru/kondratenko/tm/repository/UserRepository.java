package ru.kondratenko.tm.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import ru.kondratenko.tm.entity.User;

import javax.persistence.NoResultException;
import java.util.Optional;
@Repository
public class UserRepository extends CommonRepository<User> implements IUserRepository  {
    public User currentUser;

    public UserRepository() {
        super(User.class);
    }

    @Override
    public Optional<User> update(User input) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<User> entity = Optional.empty();

        try {
            entity = Optional.ofNullable(session.createQuery("UPDATE "
                    + User.class.getName() + " u set name = :name, password = :password, firstName = :firstName, lastName = :lastName, role = :role where id = :id", User.class)
                    .setParameter("name", input.getName())
                    .setParameter("password", input.getPassword())
                    .setParameter("firstName", input.getFirstName())
                    .setParameter("lastName", input.getLastName())
                    .setParameter("role",input.getRole())
                    .setParameter("id",input.getId())
                    .getSingleResult());
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public Optional<User> findByName(String name){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<User> entity = Optional.empty();

        try {
            entity = Optional.ofNullable(session.createQuery("SELECT u FROM " + User.class.getName() + " u where u.name = :name", User.class)
                    .setParameter("name", name).getSingleResult());
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public void removeByName(String name)  {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {
            session.createQuery("DELETE u FROM " + User.class.getName() + " u where u.name = :name", User.class)
                    .setParameter("name", name);
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();
    }

}
