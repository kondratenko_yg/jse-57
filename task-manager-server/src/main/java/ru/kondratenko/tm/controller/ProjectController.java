package ru.kondratenko.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kondratenko.tm.dto.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ProjectResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.service.IProjectIService;


@RestController
@RequestMapping("/project")
public class ProjectController implements IControllerProject {

    private IProjectIService projectService;

    @Autowired
    public ProjectController(IProjectIService projectService) {
        this.projectService = projectService;
    }

    @Override
    @GetMapping(value = "/{id}", produces = "application/json")
    public ProjectResponseDTO viewById(@PathVariable Long id)  {
        return projectService.findById(id);
    }

    @Override
    @GetMapping(value = "/all", produces = "application/json")
    public ListProjectResponseDTO list()  {
        return projectService.findAll();
    }

    @Override
    @GetMapping(value = "/view/index/{index}", produces = "application/json")
    public ProjectResponseDTO viewByIndex(@PathVariable Integer index) {
        return projectService.findByIndex(index);
    }

    @Override
    @GetMapping(value = "/view/name/{name}", produces = "application/json")
    public ListProjectResponseDTO viewByName(@PathVariable String name) {
        return projectService.findByName(name);
    }

    @Override
    @PostMapping(value = "/create", produces = "application/json",consumes = "application/json")
    public ProjectResponseDTO create(@RequestBody Project project) {
        return projectService.create(project);
    }

    @Override
    @PutMapping(value ="/update/index/{index}", produces = "application/json",consumes = "application/json")
    public ProjectResponseDTO updateByIndex(@PathVariable Integer index,@RequestBody Project project) {
        return projectService.updateByIndex(index,project);
    }

    @Override
    @PutMapping(value ="/update/id/{id}", produces = "application/json",consumes = "application/json")
    public ProjectResponseDTO updateById(@PathVariable Long id,@RequestBody Project project) {
        return projectService.updateById(id,project);
    }

    @Override
    @DeleteMapping(value ="/remove/id/{id}", produces = "application/json",consumes = "application/json")
    public ProjectResponseDTO removeById(@PathVariable Long id) {
        return projectService.removeById(id);
    }

    @Override
    @DeleteMapping(value ="/remove/index/{index}", produces = "application/json",consumes = "application/json")
    public ProjectResponseDTO removeByIndex(@PathVariable Integer index) {
        return projectService.removeByIndex(index);
    }

}
