package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.dto.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ProjectResponseDTO;
import ru.kondratenko.tm.entity.Project;


public interface IControllerProject {

    ProjectResponseDTO create(Project project);

    ProjectResponseDTO updateByIndex(Integer index, Project project);

    ProjectResponseDTO updateById(Long id, Project project);

    ListProjectResponseDTO viewByName(String name);

    ProjectResponseDTO viewById(Long id);

    ProjectResponseDTO viewByIndex(Integer index);

    ProjectResponseDTO removeByIndex(Integer index);

    ProjectResponseDTO removeById(Long id);

    ListProjectResponseDTO list();
}
