package ru.kondratenko.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kondratenko.tm.dto.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.TaskResponseDTO;
import ru.kondratenko.tm.service.IProjectTaskService;
import ru.kondratenko.tm.service.ITaskIService;
import ru.kondratenko.tm.service.ProjectTaskService;


@RestController
@RequestMapping("/task")
public class TaskController implements IControllerTask {

    private IProjectTaskService projectTaskService;

    private ITaskIService taskService;

    @Autowired
    public TaskController(ITaskIService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    @GetMapping(value = "/{id}", produces = "application/json")
    public TaskResponseDTO viewById(@PathVariable Long id)  {
        return taskService.findById(id);
    }

    @Override
    @GetMapping(value = "/all", produces = "application/json")
    public ListTaskResponseDTO list()  {
        return taskService.findAll();
    }

    @Override
    @GetMapping(value = "/view/index/{index}", produces = "application/json")
    public TaskResponseDTO viewByIndex(@PathVariable Integer index) {
        return taskService.findByIndex(index);
    }

    @Override
    @GetMapping(value = "/view/name/{name}", produces = "application/json")
    public ListTaskResponseDTO viewByName(@PathVariable String name) {
        return taskService.findByName(name);
    }

    @Override
    @PostMapping(value = "/create", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO create(@RequestBody TaskDTO task) {
        return taskService.create(task);
    }

    @Override
    @PutMapping(value ="/update/index/{index}", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO updateByIndex(@PathVariable Integer index,@RequestBody TaskDTO task) {
        return taskService.updateByIndex(index,task);
    }

    @Override
    @GetMapping(value = "/view", produces = "application/json")
    public ListTaskResponseDTO listTaskByProjectId(@RequestParam(name = "projectid") Long projectId) {
        return taskService.findAllByProjectId(projectId);
    }

    @Override
    @PutMapping(value = "/add/toproject", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO addTaskToProjectByIds(@RequestParam(name = "projectid") Long projectId,
                                                 @RequestParam(name = "taskid") Long taskId) {
        return projectTaskService.addTaskToProject(projectId,taskId);
    }

    @Override
    @PutMapping(value = "/remove/fromproject", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO removeTaskFromProjectByIds(@RequestParam(name = "projectid") Long projectId,
                                                      @RequestParam(name = "taskid") Long taskId) {
        return projectTaskService.removeTaskFromProject(projectId,taskId);
    }

    @Override
    @PutMapping(value ="/update/id/{id}", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO updateById(@PathVariable Long id,@RequestBody TaskDTO task) {
        return taskService.updateById(id,task);
    }

    @Override
    @DeleteMapping(value ="/remove/id/{id}", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO removeById(@PathVariable Long id) {
        return taskService.removeById(id);
    }

    @Override
    @DeleteMapping(value ="/remove/index/{index}", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO removeByIndex(@PathVariable Integer index) {
        return taskService.removeByIndex(index);
    }

}
