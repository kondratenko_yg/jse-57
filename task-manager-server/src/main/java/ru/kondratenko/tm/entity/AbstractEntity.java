package ru.kondratenko.tm.entity;

import lombok.Data;

import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class AbstractEntity {
    public static final Long serialVersionUID = 1L;

    private Long id;

    private String name;
}
