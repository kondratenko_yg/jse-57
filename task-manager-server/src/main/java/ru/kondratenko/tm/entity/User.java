package ru.kondratenko.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.tm.enumerated.Role;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users",uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class User extends AbstractEntity  implements Serializable {
    public static final Long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Long id = System.nanoTime();

    private String name = "";

    private String password;

    private String firstName = "";

    private String lastName = "";

    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;
}
