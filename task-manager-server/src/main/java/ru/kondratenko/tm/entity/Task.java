package ru.kondratenko.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tasks")
public class Task extends AbstractEntity  implements Serializable {
    public static final Long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Project project;

    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private User user;

    @Column(name = "birth_date", columnDefinition = "TIMESTAMP")
    private LocalDateTime deadline  = LocalDateTime.now().plusMinutes(480L);
}
