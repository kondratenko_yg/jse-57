package ru.kondratenko.tm.service;

import ru.kondratenko.tm.dto.*;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.exception.NotFoundException;
import ru.kondratenko.tm.exception.UserNotFoundException;

import java.util.Optional;

public interface IUserIService extends IService<User, UserResponseDTO, ListUserResponseDTO> {
    UserResponseDTO updateByName(final String name,final User user) throws NotFoundException;
    boolean checkPassword(final Optional<User> user, final String password);
    User getCurrentUser();
    void setCurrentUser(User user);
    UserResponseDTO findByName(final String name) throws UserNotFoundException;
    UserResponseDTO removeByName(final String name) throws UserNotFoundException;
}
