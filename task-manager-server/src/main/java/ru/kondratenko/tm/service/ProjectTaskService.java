package ru.kondratenko.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kondratenko.tm.dto.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.TaskResponseDTO;
import ru.kondratenko.tm.dto.mapper.TaskDTOMapper;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.repository.ITaskProjectRepository;
import ru.kondratenko.tm.repository.ITaskRepository;
import ru.kondratenko.tm.repository.ProjectRepository;
import ru.kondratenko.tm.repository.TaskRepository;

import java.util.List;
import java.util.Optional;
@Service
public class ProjectTaskService implements IProjectTaskService{

    private ITaskProjectRepository<Project> projectRepository;

    private ITaskRepository taskRepository;

    @Autowired
    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }
    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public TaskResponseDTO removeTaskFromProject(final Long projectId, final Long taskId) {
        final Optional<Task> task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (!task.isPresent()) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        task.get().setProject(null);
        taskRepository.update(task.get());
        return TaskResponseDTO.builder().payloadTask(TaskDTOMapper.toDto(task.get())).status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO addTaskToProject(final Long projectId, final Long taskId){
        final Optional<Project> project = projectRepository.findById(projectId);
        if (!project.isPresent()) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        final Optional<Task> task = taskRepository.findById(taskId);
        if (!task.isPresent()) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        task.get().setProject(project.get());
        taskRepository.update(task.get());
        return TaskResponseDTO.builder().payloadTask(TaskDTOMapper.toDto(task.get())).status(Status.OK).build();
    }

    @Override
    public ListTaskResponseDTO findAllByProjectId(Long projectId) {
        List<Task> listTask = taskRepository.findAllByProjectId(projectId);
        if(listTask == null || listTask.size() == 0){
            return ListTaskResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        return  ListTaskResponseDTO
                .builder()
                .payloadTask(listTask.stream().map(TaskDTOMapper::toDto).toArray(TaskDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public void clear() {
        projectRepository.clear();
        taskRepository.clear();
    }

}
